/*
** get_next_line.h for  in /home/duques_g/Programmation/get_next_line
** 
** Made by duques_g
** Login   <duques_g@epitech.net>
** 
** Started on  Tue Nov 12 05:18:54 2013 duques_g
** Last update Sun Nov 24 19:58:44 2013 duques_g
*/

#ifndef GET_NEXT_LINE_H_
# define GET_NEXT_LINE_H_

# define BUFF_SIZE 1024

#endif /* !GET_NEXT_LINE_H_ */
