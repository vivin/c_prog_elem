/*
** gnl.c for  in /home/duques_g/Programmation/get_next_line
** 
** Made by duques_g
** Login   <duques_g@epitech.net>
** 
** Started on  Sat Nov 23 06:32:46 2013 duques_g
** Last update Sun Nov 24 19:58:35 2013 duques_g
*/

#include <unistd.h>
#include <stdlib.h>
#include "get_next_line.h"

int		my_strlen(char *str)
{
  int		i;

  i = 0;
  while (str[i])
    i = i + 1;
  return (i);
}

char		*my_str_realloc(char *str, int size)
{
  int		i;
  char		*cpy;

  i = 0;
  if ((cpy = malloc(sizeof(char) * (size + 1))) == NULL)
    return (NULL);
  while (str[i] != 0)
    {
      cpy[i] = str[i];
      i = i + 1;
    }
  while (i < size)
    {
      cpy[i] = 0;
      i = i + 1;
    }
  cpy[i] = 0;
  free(str);
  return (cpy);
}

char		*my_adapt_str_cpy(char *s1, char *s2, int i)
{
  int		j;

  j = 0;
  while (s2[i])
    {
      s1[j] = s2[i];
      j = j + 1;
      i = i + 1;
    }
  s1[j] = 0;
  return (s1);
}

int		my_save(char *buffer, char *save, char **line)
{
  int		i;
  int		j;

  i = my_strlen(*line);
  j = 0;
  while (buffer[j] != 0)
    {
      if (buffer[j] == '\n')
  	{
  	  *(*line + i) = 0;
  	  save = my_adapt_str_cpy(save, buffer, j + 1);
  	  return (1);
  	}
      *(*line + i) = buffer[j];
      i = i + 1;
      j = j + 1;
    }
  save[0] = 0;
  *(*line + i) = 0;
  if (*buffer == *save)
    return (0);
  else if ((*line = my_str_realloc(*line, i + BUFF_SIZE + 2)) == NULL)
    return (-1);
  return (0);
}

char		*get_next_line(const int fd)
{
  static char	save[BUFF_SIZE + 1] = "\0";
  char		buffer[BUFF_SIZE + 1];
  char		*line;
  int		ret;

  if (!(line = malloc(BUFF_SIZE * 40 + 1 * sizeof(char))))
    return (NULL);
  line[0] = 0;
  if ((ret = my_save(save, save, &line)) != 0)
    return (line);
  if (ret < 0)
    return (NULL);
  while ((ret = read(fd, buffer, BUFF_SIZE)) > 0)
    {
      buffer[ret] = 0;
      if ((ret = my_save(buffer, save, &line)) == 1)
	return (line);
      if (ret < 0)
	return (NULL);
    }
  if (ret < 0)
    return (NULL);
  if (line[0] != 0)
    return (line);
  return (NULL);
}
